def calculate_probabilities(input_string: str) -> dict:
    """
    Calculates the probabilities of each character in the input string.

    :param input_string: The input string for which probabilities are calculated.
    :type input_string: str
    :return: A dictionary where keys are characters and values are their probabilities.
    :rtype: dict
    """
    char_count = len(input_string)
    char_probabilities = {}

    for char in input_string:
        # Check if the character is already in the dictionary
        # If yes, increment the probability; if no, add it to the dictionary
        char_probabilities[char] = char_probabilities.get(char, 0) + 1 / char_count

    return char_probabilities

if __name__ == "__main__":
    # Get user input for the input string
    input_str = input("Enter a string: ")
    
    # Call the function and print the result
    result = calculate_probabilities(input_str)
    print(result)
