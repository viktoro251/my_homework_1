# Probability Calculator

This Python script provides a function to calculate the probabilities of each character in a given input string.

## Usage

1. Clone the repository:

   ```bash
   git clone https://github.com/your-username/your-repository.git


**1.Navigate to the project directory:**
    cd your-repository

**2.Run the script:**
    python your_script.py

**3.Enter a string when prompted.**


**Function Description**
The calculate_probabilities function takes a string as input and returns a dictionary where keys are characters and values are their probabilities of occurrence in the input string.

**Example**
    from your_script import calculate_probabilities

    input_str = "hello"
    result = calculate_probabilities(input_str)
    print(result)
**This will output something like:**
    {'h': 0.2, 'e': 0.2, 'l': 0.4, 'o': 0.2}

**Feel free to customize the usage instructions and examples based on the specifics of your project.**

